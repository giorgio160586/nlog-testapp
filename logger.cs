﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog_TestApp
{
    public static class logger
    {
        private static NLog.Logger nlogger = NLog.LogManager.GetCurrentClassLogger();

        public static void Debug(String message, LogP prop)
        {
            WriteLog(LogLevel.Debug, message, null, prop);
        }

        public static void Debug(String message)
        {
            WriteLog(LogLevel.Debug, message);
        }

        public static void Fatal(String message)
        {
            WriteLog(LogLevel.Fatal, message);
        }

        public static void Fatal(String message, LogP prop)
        {
            WriteLog(LogLevel.Fatal, message, null, prop);
        }

        public static void Fatal(Exception ex, String message)
        {
            WriteLog(LogLevel.Fatal, message, ex);
        }

        public static void Fatal(Exception ex, String message, LogP prop)
        {
            WriteLog(LogLevel.Fatal, message, ex, prop);
        }

        public static void Fatal(Exception ex)
        {
            WriteLog(LogLevel.Fatal, null, ex);
        }

        public static void Fatal(Exception ex, LogP prop)
        {
            WriteLog(LogLevel.Fatal, null, ex, prop);
        }

        public static void Error(String message)
        {
            WriteLog(LogLevel.Error, message);
        }

        public static void Error(String message, LogP prop)
        {
            WriteLog(LogLevel.Error, message, null, prop);
        }

        public static void Error(Exception ex, String message)
        {
            WriteLog(LogLevel.Error, message, ex);
        }

        public static void Error(Exception ex, String message, LogP prop)
        {
            WriteLog(LogLevel.Error, message, ex, prop);
        }

        public static void Error(Exception ex)
        {
            WriteLog(LogLevel.Error, null, ex);
        }

        public static void Error(Exception ex, LogP prop)
        {
            WriteLog(LogLevel.Error, null, ex, prop);
        }

        public static void Info(String message)
        {
            WriteLog(LogLevel.Info, message);
        }

        public static void Info(String message, LogP prop)
        {
            WriteLog(LogLevel.Info, message, null, prop);
        }

        public static void Trace(String message)
        {
            WriteLog(LogLevel.Trace, message);
        }

        public static void Trace(String message, LogP prop)
        {
            WriteLog(LogLevel.Trace, message, null, prop);
        }



        public static void WriteOutput(LogP input)
        {

        }

        private static void WriteLog(NLog.LogLevel level,
                                     String message,
                                     System.Exception exception = null,
                                     LogP prop = null
                                    )
        {
            String loggerName = String.Empty;
            NLog.Logger theLogger = NLog.LogManager.GetLogger(loggerName);

            NLog.LogEventInfo theEvent = new NLog.LogEventInfo(level, loggerName, message);

            theEvent.Properties.Add("Message", message);

            if (prop != null)
            {
                theEvent.Properties.Add("UserName", prop.UserName);
            }


            theEvent.Properties.Add("StackTrace", Environment.StackTrace);

            if (exception != null)
            {
                theEvent.Exception = exception;
                theEvent.Properties["Message"] = message + exception.Message;
                theEvent.Properties["StackTrace"] = message + exception.StackTrace;
            }

            theLogger.Log(theEvent);
        }
    }

    public class LogP
    {
        public String UserName { get; set; }
    }
}
